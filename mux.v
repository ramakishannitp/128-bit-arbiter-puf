`timescale 10ns / 1ns
module mux(out,i0,i1,sel);
   input i0,i1,sel;
   output out;
//code for mux
   assign out=sel?i1:i0;
endmodule
