# 128 bit arbiter puf
The main heart lies in the python file. When the python file is executed it generates all the .v files,  .xdc file , .tcl file except the LFSR.v file. It automaticallly runs the tcl file which creates a folder that is named by the user accordingly which contains the vivado project that has been created. When the vivado project has been created, it has included the setting of FPGA, generating VIO and clocking wizard IPs, adding all the files except LFSR.v.

Steps to run python file:
1. In the terminal type python arbpuf.py
2. Then it asks to input the value of N. Type the N bit arbiter PUF that is needed. For example type 128
3. After that it asks for location where the vivado project needs to be created. Type the location in inverted commas. For example type "/home/kishan/Desktop/puf"
4. Then it asks for the name of the project that needs to be created. Type the name in inverted commas. For example # For example type "arbpuf_128bit" 

Thus all the verilog files,xdc file,tcl file and the vivado project is created.

The generated files from the python code are also included here in this folder as it is easy for viewing purpose.

The generated folder of the vivado project can be seen in the image in this folder.

