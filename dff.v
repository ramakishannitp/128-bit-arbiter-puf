`timescale 10ns / 1ns
module dff(q,clk,d);
   input d,clk;
   output reg q;
//code for d flipflop
   always@(posedge clk)
       q=d;
endmodule
