`timescale 10ns / 1ns
module arbmain(clock,out);
   input clock;
   output out;
   wire en,clk_gen;
   wire [127:0] c;
//clocking wizard module instantiation
   clk_wiz_0 v0(clk_gen,clock);
//arbiter puf module instantiation
   arb_puf a0(out,en,clk_gen,c);
//VIO instantiation
   vio_0 v1(clk_gen,out,en,c);
endmodule
