#creating vivado project with part number
create_project arbpuf_128bit {/home/kishan/Desktop/final_code/arbpuf_128bit} -part xc7a100tcsg324-1
#selecting FPGA board 
set_property board_part digilentinc.com:arty-a7-100:part0:1.0 [current_project]
#
add_files -norecurse {/home/kishan/Desktop/final_code/arbmain.v}
add_files -norecurse {/home/kishan/Desktop/final_code/pulse.v}
add_files -norecurse {/home/kishan/Desktop/final_code/arb_puf.v}
add_files -norecurse {/home/kishan/Desktop/final_code/mux.v}
add_files -norecurse {/home/kishan/Desktop/final_code/dff.v}
add_files -fileset constrs_1 -norecurse {/home/kishan/Desktop/final_code/arbpuf.xdc}
update_compile_order -fileset sources_1
set_property STEPS.SYNTH_DESIGN.ARGS.FLATTEN_HIERARCHY none [get_runs synth_1]
update_ip_catalog


#Creating VIO IP and setting the number of pins required and connecting all the pins with the respective pins of design
create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_0
set_property -dict [list CONFIG.C_NUM_PROBE_OUT {129} CONFIG.C_NUM_PROBE_IN {1}] [get_ips vio_0]
generate_target {instantiation_template} [get_files /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/vio_0/vio_0.xci]
generate_target all [get_files  /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/vio_0/vio_0.xci]
catch { config_ip_cache -export [get_ips -all vio_0] }
export_ip_user_files -of_objects [get_files /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/vio_0/vio_0.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/vio_0/vio_0.xci]
launch_runs -jobs 4 vio_0_synth_1
export_simulation -of_objects [get_files /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/vio_0/vio_0.xci] -directory /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.ip_user_files/sim_scripts -ip_user_files_dir /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.ip_user_files -ipstatic_source_dir /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.ip_user_files/ipstatic -lib_map_path [list {modelsim=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/modelsim} {questa=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/questa} {ies=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/ies} {xcelium=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/xcelium} {vcs=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/vcs} {riviera=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet


#Creating clocking wizard IP and setting the frequency
create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name clk_wiz_0
set_property -dict [list CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {20} CONFIG.USE_LOCKED {false} CONFIG.USE_RESET {false} CONFIG.MMCM_DIVCLK_DIVIDE {1} CONFIG.MMCM_CLKFBOUT_MULT_F {8.500} CONFIG.MMCM_CLKOUT0_DIVIDE_F {42.500} CONFIG.CLKOUT1_JITTER {193.154} CONFIG.CLKOUT1_PHASE_ERROR {109.126}] [get_ips clk_wiz_0]
generate_target {instantiation_template} [get_files /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci]
update_compile_order -fileset sources_1
generate_target all [get_files  /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci]
catch { config_ip_cache -export [get_ips -all clk_wiz_0] }
export_ip_user_files -of_objects [get_files /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci]
launch_runs -jobs 4 clk_wiz_0_synth_1
export_simulation -of_objects [get_files /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci] -directory /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.ip_user_files/sim_scripts -ip_user_files_dir /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.ip_user_files -ipstatic_source_dir /home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.ip_user_files/ipstatic -lib_map_path [list {modelsim=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/modelsim} {questa=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/questa} {ies=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/ies} {xcelium=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/xcelium} {vcs=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/vcs} {riviera=/home/kishan/Desktop/final_code/arbpuf_128bit/arbpuf_128bit.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet
