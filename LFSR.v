`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////


module LFSR (i_Clk,trigger,rst_n,i_Seed_Data,o_LFSR_Data,o_LFSR_Done);
   input i_Clk;
   input trigger;
   input rst_n;
     // Optional Seed
   input [127:0] i_Seed_Data;
   
   output [127:0] o_LFSR_Data;
   output o_LFSR_Done;
  reg [127:0] r_LFSR = 0;
  reg              r_XNOR;
  reg a=0;

  // Purpose: Load up LFSR with Seed if Data Valid (DV) pulse is detected.
  // Othewise just run LFSR when enabled.
  always @(posedge i_Clk)
    begin
    if (rst_n ==1)
      begin 
      if (trigger == 1'b1)
        begin
	    if (a==0)
              begin
              r_LFSR <= i_Seed_Data;
              a=a+1;
              end
        else
              begin 
//primitive polynomial
              r_LFSR = r_LFSR[0] ?     
		 { 1'd1,
		 ~r_LFSR[127:126],
		 r_LFSR[125:122],
		 ~r_LFSR[121],
		 r_LFSR[120:1] } :
	       { 1'd0, r_LFSR[127:1] } ;
              end
    
        end  
      end
    else
      begin
        r_LFSR = 128'h00000000000000000000000000000000;
      end
    end 
         

            
            //r_LFSR <= {r_LFSR[128-1:1], r_XNOR};
	    
 
 
  assign o_LFSR_Data = r_LFSR[127:0];
 
  // Conditional Assignment (?)
  assign o_LFSR_Done = (r_LFSR[127:0] == i_Seed_Data || r_LFSR[127:0] == 0 ) ? 1'b0 : 1'b1;
 
endmodule // LFSR

