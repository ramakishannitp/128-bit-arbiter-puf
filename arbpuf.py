import os
N=input("Enter value of N:")
y=input("Enter the path in doubles quotes to store all the files in that location:")
# For example type "/home/kishan/Desktop/puf"
z=input("choosethe name of the vivado project by entering in double quotes:")
# For example type "project_1"



#$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


with open (str(y)+"/dff.v","a") as d:
    # d flip flop module
    d.write("`timescale 10ns / 1ns"+"\n")
    d.write("module dff(q,clk,d);"+"\n")
    d.write("   input d,clk;"+"\n")
    d.write("   output reg q;"+"\n")
    d.write("//code for d flipflop"+"\n")
    d.write("   always@(posedge clk)"+"\n")
    d.write("       q=d;"+"\n")
    d.write("endmodule"+"\n")
    #d.write is used to write in to the file mentioned above                        

with open (str(y)+"/mux.v","a") as c:
        
    # mux module    
    c.write("`timescale 10ns / 1ns"+"\n")
    c.write("module mux(out,i0,i1,sel);"+"\n")
    c.write("   input i0,i1,sel;"+"\n")
    c.write("   output out;"+"\n")
    c.write("//code for mux"+"\n")
    c.write("   assign out=sel?i1:i0;"+"\n")
    c.write("endmodule"+"\n")
    #c.write is used to write in to the file mentioned above  

with open (str(y)+"/pulse.v","a") as b:

    #pulse module
    b.write("`timescale 10ns / 1ns"+"\n")
    b.write("//This code is used to generate a pulse which inturn used as input to the arbiter puf"+"\n") 
    b.write("module pulse(clk,en,out);"+"\n")
    b.write("//enable is there to give the input whenever the user want"+"\n")
    b.write("   input clk,en;"+"\n")
    b.write("   output reg out;"+"\n")
    b.write("   reg a=0;"+"\n")
    b.write("//code for generating pulse"+"\n")
    b.write("   always@(posedge clk)"+"\n")
    b.write("       begin"+"\n")
    b.write("           if (en==1)"+"\n")
    b.write("           begin"+"\n")
    b.write("               if (a==0)"+"\n")
    b.write("                   out=1;"+"\n")
    b.write("               else"+"\n")
    b.write("                   out=0;"+"\n")
    b.write("               a=1;"+"\n")
    b.write("           end"+"\n")
    b.write("           else a=0;"+"\n")
    b.write("       end"+"\n")
    b.write("endmodule"+"\n")
    #b.write is used to write in to the file mentioned above  



    #arbiter puf module
with open (str(y)+"/arb_puf.v","a") as a:
    a.write("`timescale 10ns / 1ns"+"\n") 
    a.write("//This is the code of arbiter puf"+"\n")
    a.write("module arb_puf(out,en,clk_gen,c);"+"\n")
    a.write("   input clk_gen,en;"+"\n")
    a.write("   input ["+str(N-1)+":0] c;"+"\n")
    a.write("   output out;"+"\n")
    a.write("   (* KEEP = \"TRUE\" *)wire iu,il;"+"\n")
    a.write("   (* KEEP = \"TRUE\" *)wire ["+str(N-1)+":0] ou;"+"\n")
    a.write("   (* KEEP = \"TRUE\" *)wire ["+str(N-1)+":0] ol;"+"\n")
    a.write("\n")
    a.write("\n")
    a.write("//pulse module instantiation"+"\n")
    a.write("   pulse k0(clk_gen,en,iu);"+"\n")
    a.write("   pulse k1(clk_gen,en,il);"+"\n")
    a.write("//mux module instantiations"+"\n")
    a.write("   mux m00(ol[0],iu,il,c[0]);"+"\n")
    #This for loop generates all the instantiations of muxes of the lower row in the structure of arbiter puf
    for i in range(1,N):
        a.write("   mux m"+str(i)+"0(ol["+str(i)+"],ou["+str(i-1)+"],ol["+str(i-1)+"],c["+str(i)+"]);"+"\n")
    a.write("   mux m01(ou[0],il,iu,c[0]);"+"\n")
    #This for loop generates all the instantiations of muxes of the upper row in the structure of arbiter puf
    for i in range(1,N):
        a.write("   mux m"+str(i)+"1(ou["+str(i)+"],ol["+str(i-1)+"],ou["+str(i-1)+"],c["+str(i)+"]);"+"\n")
    a.write("//d flipflop module instantiation"+"\n")
    a.write("   dff d0(out,ol["+str(N-1)+"],ou["+str(N-1)+"]);"+"\n")
    a.write("endmodule"+"\n")
    #a.write is used to write in to the file mentioned above  


with open (str(y)+"/arbmain.v","a") as e:
    # arbmain module - top  module
    e.write("`timescale 10ns / 1ns"+"\n")
    e.write("module arbmain(clock,out);"+"\n")
    e.write("   input clock;"+"\n")
    e.write("   output out;"+"\n")
    e.write("   wire en,clk_gen;"+"\n")
    e.write("   wire ["+str(N-1)+":0] c;"+"\n")
    e.write("//clocking wizard module instantiation"+"\n")
    e.write("   clk_wiz_0 v0(clk_gen,clock);"+"\n")
    e.write("//arbiter puf module instantiation"+"\n")
    e.write("   arb_puf a0(out,en,clk_gen,c);"+"\n")
    e.write("//VIO instantiation"+"\n")
    e.write("   vio_0 v1(clk_gen,out,en,c);"+"\n")
    e.write("endmodule"+"\n")
    #e.write is used to write in to the file mentioned above  

with open (str(y)+"/arbpuf.xdc","a") as f:
    # xdc file    
    a=0
    b=0
    # This for loop creates and assigns the pblocks for the muxes that are present in the lower row of the structure of arbiter PUF
    for i in range(1,N+1):
        f.write("create_pblock pblock_"+str(i)+"\n")
        f.write("add_cells_to_pblock [get_pblocks pblock_"+str(i)+"] [get_cells -quiet [list a0/m"+str(i-1)+"0]]"+"\n")
        f.write("resize_pblock [get_pblocks pblock_"+str(i)+"] -add {SLICE_X"+str(a)+"Y"+str(b)+":SLICE_X"+str(a+1)+"Y"+str(b)+"}"+"\n")
        if (a==80):
            a=-2
            b=b+2
        a=a+2
        i=i+1
    c=0
    d=1
    ## This for loop creates and assigns the pblocks for the muxes that are present in the upper row of the structure of arbiter PUF
    for i in range(1,N+1):
        f.write("create_pblock pblock_"+str(N+i)+"\n")
        f.write("add_cells_to_pblock [get_pblocks pblock_"+str(N+i)+"] [get_cells -quiet [list a0/m"+str(i-1)+"1]]"+"\n")
        f.write("resize_pblock [get_pblocks pblock_"+str(N+i)+"] -add {SLICE_X"+str(c)+"Y"+str(d)+":SLICE_X"+str(c+1)+"Y"+str(d)+"}"+"\n")
        if (c==80):
            c=-2
            d=d+2
        c=c+2
        i=i+1
    if (c==80):
        c=-2
        d=d+2
    # Creating pblock for the d flipflop
    f.write("create_pblock pblock_"+str(2*N+1)+"\n")  
    f.write("add_cells_to_pblock [get_pblocks pblock_"+str(2*N+1)+"] [get_cells -quiet [list a0/d0]]"+"\n")
    f.write("resize_pblock [get_pblocks pblock_"+str(2*N+1)+"] -add {SLICE_X"+str(c)+"Y"+str(d)+":SLICE_X"+str(c+1)+"Y"+str(d)+"}"+"\n")
    f.write("set_property IOSTANDARD LVCMOS33 [get_ports clock]"+"\n")
    #assigning pin for the clock in the FPGA
    f.write("#assigning pin for the clock in the FPGA"+"\n")
    f.write("set_property PACKAGE_PIN E3 [get_ports clock]"+"\n")
    f.write("set_property IOSTANDARD LVCMOS33 [get_ports out]"+"\n")
    #assigning pin for the out port in the FPGA
    f.write("#assigning pin for the out port in the FPGA"+"\n")
    f.write("set_property PACKAGE_PIN H5 [get_ports out]"+"\n")
    # assigning the clock parameters
    f.write("#assigning the clock parameters"+"\n")
    f.write("set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]"+"\n")
    f.write("set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]"+"\n")
    f.write("set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]"+"\n")
    f.write("connect_debug_port dbg_hub/clk [get_nets clk_gen]"+"\n")
    #f.write is used to write in to the file mentioned above  



with open (str(y)+"/arbpuf.tcl","a") as g:
    # tcl file to create project, add the clocking wizard and VIO modules 
    g.write("#creating vivado project with part number"+"\n")
    #creating vivado project with part number
    g.write("create_project "+str(z)+" {"+str(y)+"/"+str(z)+"} -part xc7a100tcsg324-1"+"\n")
    g.write("#selecting FPGA board "+"\n")
    #selecting FPGA board 
    g.write("set_property board_part digilentinc.com:arty-a7-100:part0:1.0 [current_project]"+"\n")
    g.write("#"+"\n")
    g.write("add_files -norecurse {"+str(y)+"/arbmain.v}"+"\n")
    g.write("add_files -norecurse {"+str(y)+"/pulse.v}"+"\n")
    g.write("add_files -norecurse {"+str(y)+"/arb_puf.v}"+"\n")
    g.write("add_files -norecurse {"+str(y)+"/mux.v}"+"\n")
    g.write("add_files -norecurse {"+str(y)+"/dff.v}"+"\n")
    g.write("add_files -fileset constrs_1 -norecurse {"+str(y)+"/arbpuf.xdc}"+"\n")
    g.write("update_compile_order -fileset sources_1"+"\n")
    g.write("set_property STEPS.SYNTH_DESIGN.ARGS.FLATTEN_HIERARCHY none [get_runs synth_1]"+"\n")
    g.write("update_ip_catalog"+"\n")
    g.write("\n")
    g.write("\n")
    g.write("#Creating VIO IP and setting the number of pins required and connecting all the pins with the respective pins of design"+"\n")
    #Creating VIO IP and setting the number of pins required and connecting all the pins with the respective pins of design
    g.write("create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_0"+"\n")
    g.write("set_property -dict [list CONFIG.C_NUM_PROBE_OUT {"+str(N+1)+"} CONFIG.C_NUM_PROBE_IN {1}] [get_ips vio_0]"+"\n")
    g.write("generate_target {instantiation_template} [get_files "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/vio_0/vio_0.xci]"+"\n")
    g.write("generate_target all [get_files  "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/vio_0/vio_0.xci]"+"\n")
    g.write("catch { config_ip_cache -export [get_ips -all vio_0] }"+"\n")
    g.write("export_ip_user_files -of_objects [get_files "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/vio_0/vio_0.xci] -no_script -sync -force -quiet"+"\n")
    g.write("create_ip_run [get_files -of_objects [get_fileset sources_1] "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/vio_0/vio_0.xci]"+"\n")
    g.write("launch_runs -jobs 4 vio_0_synth_1"+"\n")
    g.write("export_simulation -of_objects [get_files "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/vio_0/vio_0.xci] -directory "+str(y)+"/"+str(z)+"/"+str(z)+".ip_user_files/sim_scripts -ip_user_files_dir "+str(y)+"/"+str(z)+"/"+str(z)+".ip_user_files -ipstatic_source_dir "+str(y)+"/"+str(z)+"/"+str(z)+".ip_user_files/ipstatic -lib_map_path [list {modelsim="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/modelsim} {questa="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/questa} {ies="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/ies} {xcelium="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/xcelium} {vcs="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/vcs} {riviera="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet"+"\n")
    g.write("\n")
    g.write("\n")
    g.write("#Creating clocking wizard IP and setting the frequency"+"\n")
    #Creating clocking wizard IP and setting the frequency
    g.write("create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name clk_wiz_0"+"\n")
    g.write("set_property -dict [list CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {20} CONFIG.USE_LOCKED {false} CONFIG.USE_RESET {false} CONFIG.MMCM_DIVCLK_DIVIDE {1} CONFIG.MMCM_CLKFBOUT_MULT_F {8.500} CONFIG.MMCM_CLKOUT0_DIVIDE_F {42.500} CONFIG.CLKOUT1_JITTER {193.154} CONFIG.CLKOUT1_PHASE_ERROR {109.126}] [get_ips clk_wiz_0]"+"\n")
    g.write("generate_target {instantiation_template} [get_files "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci]"+"\n")
    g.write("update_compile_order -fileset sources_1"+"\n")
    g.write("generate_target all [get_files  "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci]"+"\n")
    g.write("catch { config_ip_cache -export [get_ips -all clk_wiz_0] }"+"\n")
    g.write("export_ip_user_files -of_objects [get_files "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci] -no_script -sync -force -quiet"+"\n")
    g.write("create_ip_run [get_files -of_objects [get_fileset sources_1] "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci]"+"\n")
    g.write("launch_runs -jobs 4 clk_wiz_0_synth_1"+"\n")
    g.write("export_simulation -of_objects [get_files "+str(y)+"/"+str(z)+"/"+str(z)+".srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci] -directory "+str(y)+"/"+str(z)+"/"+str(z)+".ip_user_files/sim_scripts -ip_user_files_dir "+str(y)+"/"+str(z)+"/"+str(z)+".ip_user_files -ipstatic_source_dir "+str(y)+"/"+str(z)+"/"+str(z)+".ip_user_files/ipstatic -lib_map_path [list {modelsim="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/modelsim} {questa="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/questa} {ies="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/ies} {xcelium="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/xcelium} {vcs="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/vcs} {riviera="+str(y)+"/"+str(z)+"/"+str(z)+".cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet"+"\n")
    #g.write is used to write in to the file mentioned above  
#Coommand for running the TCL file
cmd = "vivado -nolog -mode tcl -source arbpuf.tcl"
os.system(cmd)

