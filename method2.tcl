#proc random_int { upper_limit } {
#    global myrand
#    set myrand [expr int(rand() * $upper_limit + 1)]
#    return $myrand
#}
proc dec2bin {i {width {128}}} {
    #returns the binary representation of $i
    # width determines the length of the returned string (left truncated or added left 0)
    # use of width allows concatenation of bits sub-fields

    set res {}
    if {$i<0} {
        set sign -
        set i [expr {abs($i)}]
    } else {
        set sign {}
    }
    while {$i>0} {
        set res [expr {$i%2}]$res
        set i [expr {$i/2}]
    }
    if {$res eq {}} {set res 0}

    if {$width ne {}} {
        append d [string repeat 0 $width] $res
        set res [string range $d [string length $res] end]
    }
    return $sign$res
}

proc bin2hex {bin} {
    set result ""
    set prepend [string repeat 0 [expr (4-[string length $bin]%4)%4]]
    foreach g [regexp -all -inline {[01]{4}} $prepend$bin] {
        foreach {b3 b2 b1 b0} [split $g ""] {
            append result [format %X [expr {$b3*8+$b2*4+$b1*2+$b0}]]
        }
    }
    return $result
}

open_hw_manager
connect_hw_server -allow_non_jtag
open_hw_target
current_hw_device [get_hw_devices xc7a100t_0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7a100t_0] 0]
set_property PROBES.FILE {/home/arjun/Downloads/arbmain_1.ltx} [get_hw_devices xc7a100t_0]
set_property FULL_PROBES.FILE {/home/arjun/Downloads/arbmain_1.ltx} [get_hw_devices xc7a100t_0]
set_property PROGRAM.FILE {/home/arjun/Downloads/arbmain_1.bit} [get_hw_devices xc7a100t_0]
program_hw_devices [get_hw_devices xc7a100t_0]
refresh_hw_device [lindex [get_hw_devices xc7a100t_0] 0]

expr int(srand(10000) *10000000000)
set fp [open "met2_puf1.txt" w+]
for {set i 0} {$i < 10000} {incr i} {
	  if {$i == 1000} {
		    close $fp
		    set fp [open "met2_puf2.txt" w+]
        expr int(srand(10000) *10000000000)
	  }
	   if {$i == 2000} {
		    close $fp
		    set fp [open "met2_puf3.txt" w+]
        expr int(srand(10000) *10000000000)
	  }
           if {$i == 3000} {
		    close $fp
		    set fp [open "met2_puf4.txt" w+]
        expr int(srand(10000) *10000000000)
	  }
           if {$i == 4000} {
		    close $fp
		    set fp [open "met2_puf5.txt" w+]
        expr int(srand(10000) *10000000000)
	  }
           if {$i == 5000} {
		    close $fp
		    set fp [open "met2_puf6.txt" w+]
        expr int(srand(10000) *10000000000)
	  }
           if {$i == 6000} {
		    close $fp
		    set fp [open "met2_puf7.txt" w+]
        expr int(srand(10000) *10000000000)
	  }
           if {$i == 7000} {
		    close $fp
		    set fp [open "met2_puf8.txt" w+]
        expr int(srand(10000) *10000000000)
	  }
           if {$i == 8000} {
		    close $fp
		    set fp [open "met2_puf9.txt" w+]
        expr int(srand(10000) *10000000000)
	  }
           if {$i == 9000} {
		    close $fp
		    set fp [open "met2_puf10.txt" w+]
        expr int(srand(10000) *10000000000)
	  }

    set lv1 [expr int(rand()*10000000000)]
    set lv2 [expr int(rand()*10000000000)]
    set lv3 [expr int(rand()*10000000000)]
    set lv4 [expr int(rand()*10000000000)]
		set result [expr $lv1*$lv2*$lv3*$lv4]
    set bin [dec2bin $result]
    set hex [bin2hex $bin]
    set_property OUTPUT_VALUE 0 [get_hw_probes en -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    commit_hw_vio [get_hw_probes {en} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    set_property OUTPUT_VALUE $hex [get_hw_probes c -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    commit_hw_vio [get_hw_probes {c} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    set_property OUTPUT_VALUE 1 [get_hw_probes en -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    commit_hw_vio [get_hw_probes {en} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
		after 500
		set data [get_property INPUT_VALUE [get_hw_probes out_OBUF]]
    set_property OUTPUT_VALUE 0 [get_hw_probes en -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    commit_hw_vio [get_hw_probes {en} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    set_property OUTPUT_VALUE 0 [get_hw_probes c -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    commit_hw_vio [get_hw_probes {c} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    set_property OUTPUT_VALUE 1 [get_hw_probes en -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
    commit_hw_vio [get_hw_probes {en} -of_objects [get_hw_vios -of_objects [get_hw_devices xc7a100t_0] -filter {CELL_NAME=~"v1"}]]
		after 500
		set data [get_property INPUT_VALUE [get_hw_probes out_OBUF]]

   
    puts $fp "$hex $data"
}
close $fp
exit
