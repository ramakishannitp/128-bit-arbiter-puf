`timescale 10ns / 1ns
//This code is used to generate a pulse which inturn used as input to the arbiter puf
module pulse(clk,en,out);
//enable is there to give the input whenever the user want
   input clk,en;
   output reg out;
   reg a=0;
//code for generating pulse
   always@(posedge clk)
       begin
           if (en==1)
           begin
               if (a==0)
                   out=1;
               else
                   out=0;
               a=1;
           end
           else a=0;
       end
endmodule
